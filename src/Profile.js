import React, {useState} from 'react';

const Profile = () => {

    const userName = localStorage.getItem('loginName');
    const [translations, setTranslations] = useState([]);
    
    React.useEffect(() => {
        let tmp = localStorage.getItem('translations');
        setTranslations(JSON.parse(tmp));
    }, []);

    const DisplayTranslations = () => {
        if(translations == null){
            return(<p>No translations!</p>)
        } else {
            return(
                <div>
                    <ul>
                        {(translations).map((item, i) => (
                        <li key={i}>{item}

                        </li>
                        ))}
                    </ul>
                </div>
            )
        }
    }

    const logoutClick  = () => {
        localStorage.clear();
        window.location.assign('/');
    }

    return (
        <div className="box">
            <input type="submit" onClick={logoutClick} value="Log out" />
                <h2>
                    {userName}'s profile page
                </h2>
                Recent translations:
                <DisplayTranslations />
        </div>
    )
}

export default Profile;