import React, { useState } from 'react';

const Login = () => {

    const [name, setName] = useState('');

    // Redirect to /translate on click and store name
    const loginClick = e => {
        window.location.assign('/translate');
        localStorage.setItem('loginName', name);
    }

    // Can also login by pressing enter
    const onEnterPressed = e => {
        if(e.key === 'Enter'){
            loginClick();
            e.preventDefault();
        }
    }

    const onNameChange = e => setName(e.target.value);

    return(
            <div className="box">
                <h1>Lost in translation?</h1>
                <img id="scarlett" alt="LIT" src="https://www.michigandaily.com/sites/default/files/styles/large/public/160126/lost%20in%20translation.jpg?itok=tisw8oFF" />
                <input type="text" value={name} onKeyDown={onEnterPressed} onChange={onNameChange} placeholder="What is your name?" />
                <input type="submit" onClick={loginClick} name="" value="Log in" />
            </div>
    )
}
export default Login