export const getUrlObject = () => {
    const alphabet = ("abcdefghijklmnopqrstuvwxyz").split("");
    const baseUrl = "https://wpclipart.com/sign_language/American_ABCs/";
    let links = {};
    alphabet.forEach(letter => {
        links[letter] = baseUrl + letter + ".png";
    });
    return links;
}