import React, {useState} from 'react';
import {getUrlObject} from './UrlList';

const TranslatePage = () => {

    const userName = localStorage.getItem('loginName');
    const [word, setWord] = useState(''); // for user input
    const [translated, setTranslated] = useState([]); // for translation
    const [translations, setTranslations] = useState([]);

    // const translations = localStorage.getItem('translations');
    const imageUrls = getUrlObject(); // key: letter, value: image url
    
    const onWordChange = e => {
        setWord(e.target.value);
    }

    // Translate also when pressing Enter key
    const keyTranslate = e => {
        if(e.key === 'Enter'){
            e.preventDefault();
            Translate();
        }
    }

    const Translate = () => {

        let tmp = [];
        // translate input
        word.toLowerCase().split('').forEach(letter => {
            if(letter.match(/[a-z]/i)){
                tmp.push(imageUrls[letter]);
            }
        })
        setTranslated(tmp);
        // Push to translations array in local storage
        // setTranslations is asynchronous!
        tmp = translations;
        if(tmp.length === 10){
            tmp.pop();
            tmp.unshift(word);
        } else {
            tmp.push(word);
            // setTranslations(prev => [...prev, word]);
        }
        localStorage.setItem('translations', JSON.stringify(tmp));
        setTranslations(tmp);
    }

    // Routing for profile button
    const profileClick = e => {
        window.location.assign('/profile');
    }

    return(
        <div className="box">
            <input type="submit" onClick={profileClick} value="My Profile" />
            <h2>Hello, {userName}. Type a word and hit 'Translate'</h2>
            <input onChange={onWordChange} onKeyDown={keyTranslate} type="text" placeholder="Enter a word" />
            <input type="submit" onClick={Translate} value="Translate" />
        
            <div className="translation-box">
                <h3>Translated:</h3>
                {(translated).map ((line, i) => (
                    <img src={line} alt="sign" key={i}/>
                    ))}
            </div>
        </div>
    )
}
export default TranslatePage;