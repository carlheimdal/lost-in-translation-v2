import React from 'react';
import TranslatePage from './Translate';
import Login from './Login';
import Profile from './Profile';
import './App.css';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {

  // const imageUrls = getUrlObject(); // key: letter, value: image url

  return (
    <div className="App">
      <Switch>
        
        <Route exact path='/'>
          <Redirect to='/login' />
        </Route>
        <Route path='/translate' component={TranslatePage} />
        <Route path='/login' component={Login} />
        <Route path='/profile' component={Profile} />

      </Switch>
    </div>
  );
}

export default App;
